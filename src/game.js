import * as PIXI from 'pixi.js';
import 'pixi-spine';
import 'gsap';
import { map, each, sum, filter } from 'lodash';

//import from core
import app from './core/app';
import state from './core/state';
import utils from './core/utils';
import ui from './core/ui';
import sound from './core/sound';
import panel from './core/panel'
//import for slot animation
import slotInOutPP from './core/slotInOutPP';

//import for api
import apiHelper from './apihelper';

//import for bonus if bonus exist
/*
import bonus from './bonus';
*/

//#region Game Variables
let loader = PIXI.loader;
let loaded = loader.resources;
let isMobile = app.isMobile();
//#endregion

// declare variable

// declare for init core item
let uiPanel;

// declare for default game speed
let speed = 1;


function init() {
    //set placeholder for init
    state.set('balance', 0);
    state.set('betAmount', 100);
    state.set('betLine', 1);
    state.set('totalBetNum', 100);

    // declare variable for non public

    // initialise asset

    // adding to stage
    //example-> app.stage.addChild(abc);

    // init for core item and function
    uiPanel = panel.init({
        spinFn: gameSpin,
        betLineOptions: [1, 2, 3, 4, 5],
        spinButtonY: isMobile ? 15 : 634,
        turboButtonX: isMobile ? -180 : 1224,
        turboButtonY: isMobile ? 163 : 590,
        autoSpinButtonX: isMobile ? 180 : 1224,
        autoSpinButtonY: isMobile ? 163 : 670,
        spinAreaConfigX: isMobile ? 360 : 0,
        spinAreaConfigY: isMobile ? 600 : 0,
        spinAreaConfigWidth: isMobile ? 720 : 0,
        spinAreaConfigHeight: isMobile ? 550 : 0,
        betLinesUsage: true,
        gotBonusOffset: true,
    });

    uiPanel.on('infoButtonClicked', () => {

    });

    uiPanel.on('betLineChanged', (betLine) => {

    });

    uiPanel.on('betMaxButtonClicked', () => {

    });

    uiPanel.on('spinButtonClicked', () => {

    });

    uiPanel.on('turboButtonClicked', (speedUp) => {
        speed = speedUp ? 3 : 1;

        //insert slot speed
        //example-> slots.setAccelerationSpeed(speed);
    })

    apiHelper.getStartBalance().then(function (balance) {

        // make sure balance is not undefined
        balance = balance ? balance : 0;

        state.set('balance', balance);
        panel.updateText();
        uiPanel.enableActionButtons();
    });

    // bonus init if bonus exist
    /*
    bonus.init();
    */
}

export function initSound() {
    // init core sound
    sound.addSound('slotSpinningSound', loaded.slotSpinningSound.sound, {loop: true, volume: 1});
    sound.addSound('slotSpinningStopSound', loaded.slotSpinningStopSound.sound, {loop: false, volume: 1});
    sound.addSound('smallWinSound', loaded.smallWinSound.sound, {loop: false, volume: 1});
    sound.addSound('bigWinSound', loaded.bigWinSound.sound, {loop: false, volume: 1});
    sound.addSound('winMoneySound', loaded.moneySound.sound, {loop: false, volume: 1});
    sound.addSound('clickSound', loaded.buttonClickSound.sound, {loop: false, volume: 1});
    sound.addSound('bonusStarter', loaded.bonusTriggerSound.sound, {loop: false, volume: 1});

    // add in sound that didnt have in core such as background sound
}

function gameSpin(isAutoPlay = false) {

    // slot spinning
    //example-> slots.startSpin();

    sound.play('slotSpinningSound');

    return Promise.all([
        // set slot to get spin data from backend
        apiHelper.getSpinData(state.get('betAmount'), state.get('betLine'), isAutoPlay),
        new Promise((resolve) => {
            setTimeout(resolve, 1000 / speed);
        })
    ]).then(([data]) => {
        each(slots, (slot, index) => {
            // after get data, stop spin
            TweenMax.delayedCall((0.25 * index) / speed, () => {
                const combination = data.slots[index];
                slot.stopSpin(combination);
            });
        });

        // after animation stop
        return (new Promise((resolve) => {
            setTimeout(() => {
                sound.stop('slotSpinningSound');
                sound.play('slotSpinningStopSound');
                checkWin(data).then((payout) => {
                    resolve(payout);
                });
            }, 1500 / speed);
        }));
    });
}

function checkWin(data) {
    // check payout. if have payout, continue, if not, resolve promise and end check win
    if (payout <= 0) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(payout);
            }, 1200 / speed);
        });
    }

    // declare payout data
    //example-> const payout = data.payout;

    // Show lines that win using promise
    //example->
    /*
    let showLine = new Promise((resolve) => {
        // after all line is show
            return resolve();
    }
    */

    // go to bonus if bonus exist
}

// create function to support game here

export default {
    init,
    initSound
}
