import * as PIXI from 'pixi.js';

import app from './core/app';
import state from './core/state';
import apiHelper from './apihelper';
import sound from './core/sound';
import panel from './core/panel';
import ui from './core/ui';
import utils from './core/utils';
import { initSound } from "./game";

const loaded = PIXI.loader.resources;
const isMobile = app.isMobile();

//if bonus exist, init bonus and fun bonus function here.

// declare variable

//init bonus asset
function init() {

    // declare variable for non public

    // initialise asset

    // adding to stage
    //example-> app.stage.addChild(abc);

}

// create function to support bonus game here

export default {
    init,
}
