import apiService from './core/api';

function getStartBalance() {
    return apiService.post('/auth/member/me').then((data) => {
        return data.balance;
    });
}

function getSpinData(betAmount, bet_line, isAutoplay) {
    let data = {bet_amount: betAmount, bet_line: bet_line, isAutoPlay: isAutoplay};
    return apiService.post('/play/seed-project', data);
}

function postBonusValue(token, value) {
    let data = {bonusToken: token, value: value};
    return apiService.post('/bonus', data);
}

export default {
    getStartBalance,
    getSpinData,
    postBonusValue,
}
