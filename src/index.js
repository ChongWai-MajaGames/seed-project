import app from './core/app';
import game from './game';
import utils from "./core/utils";

PIXI.utils.sayHello('seed-project');

let isMobile = app.isMobile();
let loader = app.getLoader();
let lang = utils.getQueryParam('lang') || 'en';

//! shared asset
loader
//! Slot totem default
//example-> .add('abc', './assets/abc.png')



//! Calling loader to load all out assets based on mobile or non-mobile
if (isMobile) {
    loader

}
else if (!isMobile) {
    loader

}

/*
item usually needed are
- background
- slot texture
- paytable
*/

// for changing loading bar colour
/*
loader
    .on('start', function () {
        app.loadingStart({
            loadingRectColor: 0x2760EE,
        });
    })
*/

loader.on('complete', function () {
    game.initSound();
    game.init();
}).load();
